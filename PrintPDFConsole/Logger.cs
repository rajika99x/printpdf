﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintPDF
{
    class Logger
    {

        const string publicFolderPath = @"C:\Users\Public\RamBase\printpdf\";

        public static void Log(string message)
        {
            if (string.IsNullOrEmpty(message.Trim())) return;

            bool exists = System.IO.Directory.Exists(publicFolderPath);
            if (!exists)
                System.IO.Directory.CreateDirectory(publicFolderPath);

            var logFilename = publicFolderPath + "printPdf.log";


            try
            {
                using (FileStream fs = new FileStream(logFilename, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
                {
                    using (StreamWriter streamWriter = new StreamWriter(fs))
                    {
                        streamWriter.WriteLine("[" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff") + "] " + message);
                        streamWriter.Close();
                    }
                }
            }
            catch (Exception e)
            {
                throw e;
            }

            Debug.WriteLine(message);
        }
    }
}
