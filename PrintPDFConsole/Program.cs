﻿using Datalogics.PDFL;
using PrintPDF;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PrintPDFConsole
{
    class Program
    {
        const string publicFolderPath = @"C:\Users\Public\RamBase\printpdf\";

        static void Main(string[] args)
        {
            Logger.Log("Starting Service");

            string printer = string.Empty;
            string pdfFile = string.Empty;
            string crashpdfFile = string.Empty;

            string configFilePath = publicFolderPath + "print.txt";
            Logger.Log("Config file path: " + configFilePath);

            try
            {
                using (StreamReader reader = new StreamReader(configFilePath))
                {
                    printer = reader.ReadLine();
                    pdfFile = reader.ReadLine();
                }
                if (string.IsNullOrEmpty(pdfFile))
                {
                    Logger.Log("Configure printer name and PDF file path in each line in the print.txt file");
                    return;
                }

                if (string.IsNullOrEmpty(pdfFile))
                    pdfFile = @"C:\Users\Public\RamBase\test.pdf";

                pdfFile = System.IO.Path.Combine(publicFolderPath, pdfFile);
            }
            catch (Exception ex)
            {
                Logger.Log(ex.Message + " " + ex.StackTrace);
            }
            printAdobe(pdfFile, printer);

        }

        //static void printSpire(string pdfFile, string printer)
        //{
        //    try
        //    {

        //        Spire.License.LicenseProvider.SetLicenseFileName("license.elic.xml");

        //        Spire.Pdf.PdfDocument pdfDocument = new Spire.Pdf.PdfDocument();
        //        pdfDocument.LoadFromFile(pdfFile);

        //        pdfDocument.PageScaling = Spire.Pdf.PdfPrintPageScaling.ActualSize;

        //        pdfDocument.PrinterName = printer;
        //        var printDocument = pdfDocument.PrintDocument;

        //        printDocument.Print();

        //        Logger.Log("Printed successfully with Spire");
        //        Logger.Log("");



        //    }
        //    catch (Exception ex)
        //    {
        //        Logger.Log("Exception occurred while printing using Spire.Pdf" + ex.Message);
        //    }
        //}

        static void printAdobe(string pdfFile, string printer)
        {
            try
            {
                Logger.Log("Printing PDF filename: " + pdfFile);
                Logger.Log("Before printing");

                using (Library lib = new Library())
                {



                    using (Document doc = new Document(pdfFile))
                    {
                        bool documentVersionIsNew = doc.MajorVersionIsNewerThanCurrentLibrary;

                        using (PrintUserParams userParams = new PrintUserParams())
                        {
                            // NOTE: userParams are only valid for ONE print job...
                            userParams.NCopies = 1;
                            userParams.ShrinkToFit = false;
                            userParams.PrintParams.ExpandToFit = false;
                            userParams.Collate = true;

                            userParams.UseDefaultPrinter(doc);
                            userParams.DeviceName = printer;

                            int lastPage = doc.NumPages;


                            PageRange pageRange = new PageRange(0, lastPage, PageSpec.AllPages);

                            List<PageRange> pageRanges = new List<PageRange>();
                            pageRanges.Add(pageRange);

                            userParams.PrintParams.PageRanges = pageRanges;
                            try
                            {
                                doc.Print(userParams);
                                Logger.Log("Printed successfully with Adobe");

                                Logger.Log("");
                            }
                            catch (Exception ex)
                            {
                                Console.Error.WriteLine(ex.Message);
                                Logger.Log("Print unsuccessfull " + ex.Message);
                                Logger.Log("");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log("Exception occurred while printing using Adobe.Pdf" + ex.Message);
            }
        }
    }
}

