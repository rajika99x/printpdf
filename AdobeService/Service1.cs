﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace AdobeService
{
    public partial class Service1 : ServiceBase
    {
        public Service1()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                string adobeExe = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location) +
                                  @"\PrintPDFConsole.exe";

                ProcessStartInfo startInfo = new ProcessStartInfo(adobeExe)
                {
                    WindowStyle = ProcessWindowStyle.Hidden,
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    RedirectStandardError = true,
                    CreateNoWindow = true
                };

                Process process = Process.Start(startInfo);
            }
            catch (Exception ex)
            {

            }
        }

        protected override void OnStop()
        {
        }
    }
}
